package com.epam.lab.publicutilities.controller;

import com.epam.lab.publicutilities.service.AdminService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({SpringExtension.class})
@WebMvcTest(AdminController.class)
class AdminControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    AdminService adminServiceMock;

    @ParameterizedTest
    @CsvSource({"/admin", "/admin/workers", "/admin/orders"})
    @SneakyThrows
    public void testGetPages(String path){
        given(adminServiceMock.getUsers()).willReturn(Collections.emptyList());
        mockMvc.perform(get(path)
                .contentType(MediaType.TEXT_HTML)).andExpect(status().isOk());
    }
}