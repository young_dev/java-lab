package com.epam.lab.publicutilities.controller;

import com.epam.lab.publicutilities.configuration.UserDetailsServiceImpl;
import com.epam.lab.publicutilities.configuration.WebSecurityConfig;
import com.epam.lab.publicutilities.model.Role;
import com.epam.lab.publicutilities.model.User;
import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({SpringExtension.class})
@WebMvcTest(LoginPageController.class)
@ContextConfiguration(classes = {WebSecurityConfig.class})
@FieldDefaults(level = AccessLevel.PRIVATE )
class SecurityTest {

    static final String ADMIN_URL = "/admin";
    static final String DISPATCHER_URL = "/dispatcher";
    static final String USER_URL = "/user/orders/";
    static final String LOGIN_URL = "http://localhost/login";
    static final String DEFAULT_ADMIN_USERNAME = "admin";
    static final String DEFAULT_ADMIN_PASSWORD = "admin";
    static final String FULL_NAME = "user";
    static final String USERNAME = "user";
    static final String LOGIN_ERROR_URL = "/login?error";
    static final String DISPATCHER = "dispatcher";
    static final String USER_PASSWORD = "user";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    PasswordEncoder passwordEncoder;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Test
    @SneakyThrows
    public void testLoginAsDefaultAdmin() {
        when(userDetailsService.loadUserByUsername(DEFAULT_ADMIN_USERNAME)).thenThrow(UsernameNotFoundException.class);
        RequestBuilder requestBuilder = formLogin().user(DEFAULT_ADMIN_USERNAME).password(DEFAULT_ADMIN_PASSWORD);
        mockMvc.perform(requestBuilder).andExpect(redirectedUrl(ADMIN_URL)).andExpect(status().isFound());
    }

    @Test
    @SneakyThrows
    public void testLoginAsDefaultAdminWIthWrongPassword() {
        when(userDetailsService.loadUserByUsername(DEFAULT_ADMIN_USERNAME)).thenThrow(UsernameNotFoundException.class);
        RequestBuilder requestBuilder = formLogin().user(DEFAULT_ADMIN_USERNAME).password("11111");
        mockMvc.perform(requestBuilder).andExpect(redirectedUrl(LOGIN_ERROR_URL)).andExpect(status().isFound());
    }

    @Test
    @SneakyThrows
    public void testAdminHasAccessToAdminPage() {
        mockMvc.perform(
                get(ADMIN_URL)
                        .with(user(DEFAULT_ADMIN_USERNAME).roles(Role.ROLE_ADMIN.getRole())))
                        .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    public void testNotAdminDoNotHasAccessToAdminPage() {
        mockMvc.perform(
                get(ADMIN_URL)
                        .with(user(USERNAME).roles(Role.ROLE_USER.getRole())))
                .andExpect(status().isForbidden());

        mockMvc.perform(
                get(ADMIN_URL)
                        .with(user(DISPATCHER).roles(Role.ROLE_DISPATCHER.getRole())))
                .andExpect(status().isForbidden());

    }

    @Test
    @SneakyThrows
    public void testUnauthorizedAccess() {
        mockMvc.perform(
                get(ADMIN_URL))
                .andExpect(redirectedUrl(LOGIN_URL))
                .andExpect(status().isFound());

        mockMvc.perform(
                get(DISPATCHER_URL))
                .andExpect(redirectedUrl(LOGIN_URL))
                .andExpect(status().isFound());

        mockMvc.perform(
                get(USER_URL))
                .andExpect(redirectedUrl(LOGIN_URL))
                .andExpect(status().isFound());
    }

    @Test
    @SneakyThrows
    public void testDispatcherHasAccessToDispatcherPage() {
        mockMvc.perform(
                get(DISPATCHER_URL)
                        .with(user(DISPATCHER).roles(Role.ROLE_DISPATCHER.getRole())))
                .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    public void testNotDispatcherDoNotHasAccessToDispatcherPage() {
        mockMvc.perform(
                get(DISPATCHER_URL)
                        .with(user(DEFAULT_ADMIN_USERNAME).roles(Role.ROLE_ADMIN.getRole())))
                .andExpect(status().isForbidden());

        mockMvc.perform(
                get(DISPATCHER_URL)
                        .with(user(USERNAME).roles(Role.ROLE_USER.getRole())))
                .andExpect(status().isForbidden());
    }

    @Test
    @SneakyThrows
    public void testLoginAsUser() {
        User user = new User(1, FULL_NAME, "SPb", Role.ROLE_USER, USERNAME,
                passwordEncoder.encode(USER_PASSWORD));
        UserDetails userDetails = new UserDetailsServiceImpl.UserDetailsImpl(user);
        when(userDetailsService.loadUserByUsername(USERNAME)).thenReturn(userDetails);
        RequestBuilder requestBuilder = formLogin().user(USERNAME).password(USER_PASSWORD);
        mockMvc.perform(requestBuilder).andExpect(redirectedUrl(SecurityTest.USER_URL))
                .andExpect(status().isFound());
    }
    @Test
    @SneakyThrows
    public void testLoginAsUserWithWrongUser() {
        when(userDetailsService.loadUserByUsername(USERNAME)).thenThrow(UsernameNotFoundException.class);
        RequestBuilder requestBuilder = formLogin().user(USERNAME).password(USER_PASSWORD);
        mockMvc.perform(requestBuilder).andExpect(redirectedUrl(LOGIN_ERROR_URL))
                .andExpect(status().isFound());
    }

}