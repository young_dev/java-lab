package com.epam.lab.publicutilities.controller;

import com.epam.lab.publicutilities.service.UserService;
import com.epam.lab.publicutilities.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Collection;

@Controller
public class LoginPageController {

    private UserService userService;

    @Autowired
    public LoginPageController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String defaultPage(Principal principal) {
        if (principal != null) {
            Object currentUser = ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
            if (currentUser instanceof UserDetails) {
                Collection<? extends GrantedAuthority> roles = ((UserDetails)currentUser).getAuthorities();
                return "redirect:" + SecurityUtils.getUserHomepage(roles);
            } else {
                throw new IllegalArgumentException("Invalid user credentials");
            }
        } else {
            return "login";
        }
    }

    @GetMapping("/register")
    public String registerUserForm() {
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@RequestParam String username,
                               @RequestParam String password,
                               @RequestParam String fullName,
                               @RequestParam(required = false) String address) {
        userService.registerUser(username, password, fullName, address);
        return "redirect:/";
    }
}
