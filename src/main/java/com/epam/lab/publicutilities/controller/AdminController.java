package com.epam.lab.publicutilities.controller;

import com.epam.lab.publicutilities.model.Role;
import com.epam.lab.publicutilities.model.User;
import com.epam.lab.publicutilities.service.AdminService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Arrays;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminController {
    AdminService adminService;

    @RequestMapping(value = "/admin/changerole/{userId}", method = RequestMethod.GET)
    public ModelAndView changeUserRole(@PathVariable String userId, ModelAndView modelAndView) {
        modelAndView.addObject("user", adminService.getUser(userId));
        modelAndView.addObject("role", Arrays.asList(Role.values()));
        modelAndView.setViewName("changerole");
        return modelAndView;
    }

    @RequestMapping(value = "/admin/changerole/{userId}", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String changeUserRole(@PathVariable("userId") String userId,
                                 @RequestParam("role") String role){
        return adminService.changeUserRole(userId, role) ? "redirect:/admin" : "redirect:/500";
    }

    @GetMapping("/admin")
    public ModelAndView admin(ModelAndView modelAndView, Principal principal) {
        User currentUser = User.getUserFromPrincipal(principal);
        modelAndView.addObject("currentUser", currentUser);
        modelAndView.addObject("users", adminService.getUsers());
        modelAndView.addObject("ADMIN", Role.ROLE_ADMIN);
        modelAndView.addObject("DISPATCHER", Role.ROLE_DISPATCHER);
        modelAndView.setViewName("admin");
        return modelAndView;
    }

    @GetMapping("/admin/workers")
    public ModelAndView workers(ModelAndView modelAndView) {
        modelAndView.addObject("workers", adminService.getWorkers());
        modelAndView.setViewName("admin_workers");
        return modelAndView;
    }

    @GetMapping("/admin/orders")
    public ModelAndView orders(ModelAndView modelAndView) {
        modelAndView.addObject("orders", adminService.getOrders());
        modelAndView.setViewName("admin_orders");
        return modelAndView;
    }

    @RequestMapping(value = "/admin/deleteuser/{userId}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable String userId, Principal principal) {
        int cId = User.getUserFromPrincipal(principal).getId();
        return cId != Integer.valueOf(userId) && adminService.deleteUser(userId) ?
                "redirect:/admin": "redirect:/500";
    }

    @RequestMapping(value = "/admin/deleteworker/{workerId}", method = RequestMethod.GET)
    public String deleteWorker(@PathVariable String workerId) {
        return adminService.deleteWorker(workerId) ? "redirect:/admin/workers": "redirect:/500";
    }

    @RequestMapping(value = "/admin/deleterequest/{requestId}", method = RequestMethod.GET)
    public String deleteRequest(@PathVariable String requestId) {
        return adminService.deleteRequest(requestId) ? "redirect:/admin/orders": "redirect:/500";
    }
}
