package com.epam.lab.publicutilities.controller;

import com.epam.lab.publicutilities.model.Status;
import com.epam.lab.publicutilities.model.User;
import com.epam.lab.publicutilities.service.UserService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserController {
    UserService userService;

    @RequestMapping(value = "/user/orders/add", method = RequestMethod.GET)
    public ModelAndView getAddingOrderPage(ModelAndView modelAndView, Principal principal) {
        modelAndView.setViewName("add-order");
        modelAndView.addObject("userId", this.getUserId(principal));
        return modelAndView;
    }

    @RequestMapping(value = "/user/orders", method = RequestMethod.GET)
    public ModelAndView getUserOrders(ModelAndView modelAndView, Principal principal) {
        String userId = String.valueOf(this.getUserId(principal));
        modelAndView.addObject("usersOrders", userService.getOrders(userId));
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("userFullName", userService.getFullName(userId));
        modelAndView.setViewName("users-orders");
        return modelAndView;
    }

    @RequestMapping(value = "/user/orders/add", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String addOrder(String description,
                           String expectedDate,
                           Principal principal) {
        String userId= String.valueOf(this.getUserId(principal));
        userService.addOrder(userId,
                description,
                Status.ADDED.name(),
                expectedDate);
        return  "redirect:/user/orders";
    }

    @RequestMapping(value = "/user/orders/change/{orderId}", method = RequestMethod.GET)
    public ModelAndView getChangingOrderPage(ModelAndView modelAndView,
                                             Principal principal,
                                             @PathVariable("orderId") String orderId) {
        int userId = this.getUserId(principal);

        modelAndView.setViewName("change-order");
        modelAndView.addObject("orderId", orderId);
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("description", userService.getOrder(orderId, String.valueOf(userId)).getDescription());
        modelAndView.addObject("fullName", userService.getFullName(String.valueOf(userId)));
        modelAndView.addObject("expectedDate", userService.getOrder(orderId, String.valueOf(userId)).getExpectedDate());
        modelAndView.addObject("status", userService.getOrder(orderId,String.valueOf(userId)).getStatus());
        return modelAndView;
    }

    @RequestMapping(value = "/user/orders/change/{orderId}", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String changeOrder(@PathVariable("orderId") String orderId,
                              String description) {
        return userService.changeOrder(orderId,
                description) ? "redirect:/user/orders"  : "redirect:/500";

    }

    private int getUserId(Principal principal){
        return User.getUserFromPrincipal(principal).getId();
    }
}
