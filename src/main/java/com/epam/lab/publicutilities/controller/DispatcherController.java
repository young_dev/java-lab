package com.epam.lab.publicutilities.controller;

import com.epam.lab.publicutilities.model.Brig;
import com.epam.lab.publicutilities.model.Status;
import com.epam.lab.publicutilities.model.WorkerStatus;
import com.epam.lab.publicutilities.service.DispatcherService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

@Controller
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DispatcherController {

    final
    DispatcherService dispatcherService;

    @Autowired
    public DispatcherController(DispatcherService dispatcherService) {
        this.dispatcherService = dispatcherService;
    }

    @GetMapping("/dispatcher")
    public ModelAndView getAllOrders(ModelAndView modelAndView) {
        modelAndView.addObject("orders", dispatcherService.getAllOrders());
        modelAndView.addObject("userIdToNameMap", dispatcherService.getUserIdNameMap());
        modelAndView.setViewName("dispatcher");
        return modelAndView;
    }

    @GetMapping("/dispatcher/workers")
    public ModelAndView getAllWorkers(ModelAndView modelAndView) {
        modelAndView.addObject("workers", dispatcherService.getAllWorkers());
        modelAndView.setViewName("dispatcherWorkers");
        return modelAndView;
    }

    @GetMapping("/dispatcher/users")
    public ModelAndView getAllUsers(ModelAndView modelAndView) {
        modelAndView.addObject("users", dispatcherService.getAllUsers());
        modelAndView.setViewName("dispatcherUsers");
        return modelAndView;
    }

    @GetMapping("/dispatcher/workplans")
    public ModelAndView getAllWorkplans(ModelAndView modelAndView) {
        modelAndView.addObject("workplans", dispatcherService.getAllWorkPlans());
        modelAndView.setViewName("dispatcherWorkplans");
        return modelAndView;
    }

    @GetMapping(value = "/dispatcher/newworkplan/{orderId}")
    public ModelAndView createNewWorkPlan(ModelAndView modelAndView, @PathVariable int orderId) {
        modelAndView.setViewName("dispatcherNewWorkplan");
        modelAndView.addObject("orderId", orderId);
        modelAndView.addObject("deadline", dispatcherService.getOrder(orderId).getExpectedDate());
        modelAndView.addObject("description", dispatcherService.getOrder(orderId).getDescription());
        return modelAndView;
    }

    @GetMapping("/dispatcher/brigades/newbrigade")
    public ModelAndView getWorkersForBrigades(ModelAndView modelAndView) {
        modelAndView.setViewName("dispatcherNewBrigade");
        modelAndView.addObject("workers", dispatcherService.getAllWorkers());

        modelAndView.addObject("formedBrigades", dispatcherService.getAllBrigades());
        return modelAndView;
    }

    @GetMapping("/dispatcher/orders/{orderId}")
    public ModelAndView changeOrderStatus(ModelAndView modelAndView, @PathVariable int orderId) {
        modelAndView.setViewName("changeOrderStatus");
        modelAndView.addObject("idOfOrder", orderId);
        modelAndView.addObject("order", dispatcherService.getOrder(orderId));
        modelAndView.addObject("status", Arrays.asList(Status.values()));
        return modelAndView;
    }

    @GetMapping("/dispatcher/workers/{workerId}")
    public ModelAndView changeWorkerStatus(ModelAndView modelAndView, @PathVariable int workerId){
        modelAndView.setViewName("dispatcherChangeWorkerStatus");
        modelAndView.addObject("workerId", workerId);
        modelAndView.addObject("worker", dispatcherService.getWorkerById(workerId));
        modelAndView.addObject("workerStatus", WorkerStatus.values());
        return modelAndView;
    }

    @PostMapping("/dispatcher/workers/{workerId}")
    public String changeWorkerStatus(@PathVariable("workerId") int workerId,
                                     @RequestParam("workerStatus") String workerStatus){
        dispatcherService.changeWorkerStatus(workerId, workerStatus);
        return "redirect:/dispatcher/workers";
    }

    @PostMapping("/dispatcher/newworkplan/{orderId}")
    public String createWorkPlan(@PathVariable int orderId, int brigId, Date deadline, String description) {
        dispatcherService.createNewWorkingPlan(orderId, brigId, deadline, description);
        return "redirect:/dispatcher";
    }

    @PostMapping("/dispatcher/brigades/newbrigade")
    public String createNewBrigade(@RequestParam("isInBrigade") List<Integer> workersIds,
                                   @RequestParam("brigadeId") int brigadeId, HttpSession httpSession) {
        httpSession.setAttribute("workersIds", workersIds);
        Brig newBrig = dispatcherService.createNewBrigade(brigadeId);
        dispatcherService.createNewFormedBrigade(workersIds, newBrig.getId());
        dispatcherService.changeWorkersStatuses(workersIds, 0);
        return "redirect:/dispatcher/brigades/formedBrigade";
    }

    @PostMapping("/dispatcher/orders/{orderId}")
    public String changeOrderStatus(@RequestParam("orderStatus") String orderStatus,
                                    @PathVariable("orderId") int orderId) {
        dispatcherService.changeOrderStatus(orderStatus, orderId);
        return "redirect:/dispatcher/";
    }

    @GetMapping("/dispatcher/brigades/formedBrigade")
    public ModelAndView getFormedBrigade(ModelAndView modelAndView, HttpSession httpSession) {
        List<Integer> workersIds = (List<Integer>) httpSession.getAttribute("workersIds");
        modelAndView.addObject("workers",dispatcherService.getWorkersListByIds(workersIds));
        modelAndView.setViewName("dispatcherFormedBrigade");
        return modelAndView;
    }

}
