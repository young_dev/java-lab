package com.epam.lab.publicutilities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublicUtilitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublicUtilitiesApplication.class, args);
	}
}
