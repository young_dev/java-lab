package com.epam.lab.publicutilities.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class Worker {
    int id;
    String name;
    String specialization;
    boolean isFree;
}
