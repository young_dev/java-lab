package com.epam.lab.publicutilities.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WorkerStatus {
    TRUE("1"),
    FALSE("0");
    private String workerStatus;
}
