package com.epam.lab.publicutilities.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class WorkingPlan {

    int id;
    int orderId;
    int brigId;
    Date deadlineDate;
    String description;

}
