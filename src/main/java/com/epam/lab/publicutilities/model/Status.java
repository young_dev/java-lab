package com.epam.lab.publicutilities.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Status {
    ADDED("ADDED"),
    IN_PROCESS("IN_PROCESS"),
    DONE("DONE");
    private String status;
}
