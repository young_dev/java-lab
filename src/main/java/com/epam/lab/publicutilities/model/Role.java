package com.epam.lab.publicutilities.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Role {
    ROLE_USER("USER"),
    ROLE_ADMIN("ADMIN"),
    ROLE_DISPATCHER("DISPATCHER");
    private String role;
}
