package com.epam.lab.publicutilities.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class Order {

    int id;
    int userId;
    String description;
    Status status;
    Date expectedDate;

}
