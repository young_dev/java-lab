package com.epam.lab.publicutilities.model;

import com.epam.lab.publicutilities.configuration.UserDetailsServiceImpl;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.security.Principal;

@Getter
@Setter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    int id;
    String fullName;
    String address;
    Role role;
    String username;
    String password;

    public User(int id, String fullName, String address, Role role) {
        this.id = id;
        this.fullName = fullName;
        this.address = address;
        this.role = role;
    }

    public User() {

    }

    public static User getUserFromPrincipal(Principal principal){
        return ((UserDetailsServiceImpl.UserDetailsImpl)
                ((UsernamePasswordAuthenticationToken)principal)
                        .getPrincipal())
                .getUser();
    }
}
