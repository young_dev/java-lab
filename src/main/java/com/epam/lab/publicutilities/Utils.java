package com.epam.lab.publicutilities;


import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;

import java.util.Properties;

public class Utils {
    @NotNull
    @SneakyThrows
    public static Properties getProperties(String fileName) {
        val properties = new Properties();
        val name = String.format("/%s.properties", fileName);
        try (val inputStream = PublicUtilitiesApplication.class.getResourceAsStream(name)) {
            properties.load(inputStream);
        }
        return properties;
    }
}