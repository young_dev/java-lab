package com.epam.lab.publicutilities.dao;

import com.epam.lab.publicutilities.model.*;

import java.sql.Date;
import java.util.List;

public interface DispatcherDao {

    List<FormedBrig> getAllBrigades();

    List<Order> getAllOrders();

    List<User> getAllUsers();

    List<Worker> getAllWorkers();

    List<WorkingPlan> getAllWorkPlans();

    Order getOrder(int orderId);

    FormedBrig createNewFormedBrigade(int workerId, int brigadeId);

    WorkingPlan createNewWorkingPlan(int orderId, int brigId, Date deadline, String description);

    Brig createNewBrigade(int brigadeId);

    boolean changeOrderStatus(String orderStatus, int orderId);

    boolean changeWorkerStatus(int workersIds, int status);

    Worker getWorkerById(int workerId);
}