package com.epam.lab.publicutilities.dao.impl;

import com.epam.lab.publicutilities.Utils;
import com.epam.lab.publicutilities.dao.UserDao;
import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.Role;
import com.epam.lab.publicutilities.model.Status;
import com.epam.lab.publicutilities.model.User;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDaoImpl extends NamedParameterJdbcTemplate implements UserDao {

    static final String ID = "ID";
    static final String USERNAME = "USERNAME";
    static final String PASSWORD = "PASSWORD";
    static final String FULLNAME = "FULLNAME";
    static final String ADDRESS = "ADDRESS";
    static final String ROLE = "ROLE";

    Properties sql = Utils.getProperties("queries/UserQuery");
    UserRowMapper mapper;

    @Value("${INSERT_USER}")
    String insertUserQuery;
    @Value("${LOAD_BY_NAME}")
    String loadByNameQuery;

    @Autowired
    public UserDaoImpl(DataSource dataSource) {
        super(dataSource);
        this.mapper = new UserRowMapper();
    }

    @Override
    public User registerUser(User user) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USERNAME, user.getUsername());
        parameterSource.addValue(PASSWORD, user.getPassword());
        parameterSource.addValue(FULLNAME, user.getFullName());
        parameterSource.addValue(ADDRESS, user.getAddress());
        parameterSource.addValue(ROLE, user.getRole().name());
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        update(insertUserQuery, parameterSource, generatedKeyHolder);
        user.setId(generatedKeyHolder.getKey().intValue());
        return user;
    }

    @Override
    public User loadByName(String username) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USERNAME, username);
        // Exception handling is required, since we want to return null when user not found
        // and not to throw some JDBC exception
        try {
            return queryForObject(loadByNameQuery, parameterSource, mapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private static class UserRowMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getInt(ID));
            user.setUsername(rs.getString(USERNAME));
            user.setRole(Role.valueOf(rs.getString(ROLE)));
            user.setAddress(rs.getString(ADDRESS));
            user.setFullName(rs.getString(FULLNAME));
            user.setPassword(rs.getString(PASSWORD));
            return user;
        }
    }

    @Override
    public List<Order> getOrders(String userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("userId", userId);

        return this.getOrdersList(sql.getProperty("GET_ORDERS"), parameterSource);
    }

    @Override
    public int addOrder(String user_id, String description, String status, String expectedDate) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("userId", user_id);
        parameterSource.addValue("description", description);
        parameterSource.addValue("status", status);
        parameterSource.addValue("expectedDate", expectedDate);

        KeyHolder keyHolder = new GeneratedKeyHolder();
        update(sql.getProperty("INSERT_NEW_ORDER"), parameterSource, keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Override
    public boolean changeOrder(String id, String description) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);
        parameterSource.addValue("description", description);

        return (update(sql.getProperty("CHANGE_ORDER"), parameterSource) == 1);
    }

    @Override
    public String getFullName(String userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("userId", userId);

        return query(sql.getProperty("GET_FULL_NAME"), parameterSource, (rS, rN) ->
                rS.getString("FULLNAME")).get(0);
    }

    @Override
    public Order getOrder(String orderId, String userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("orderId", orderId);
        parameterSource.addValue("userId", userId);

        return this.getOrdersList(sql.getProperty("GET_ORDER"), parameterSource).get(0);
    }

    private List<Order> getOrdersList(String sql, MapSqlParameterSource params){
        return query(sql, params, (rS, rN) -> new Order(
                rS.getInt("ID"),
                rS.getInt("USER_ID"),
                rS.getString("DESCRIPTION"),
                Status.valueOf(rS.getString("STATUS")),
                rS.getDate("EXPECTED_DATE")));
    }
}
