package com.epam.lab.publicutilities.dao;

import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.Status;
import com.epam.lab.publicutilities.model.User;
import java.util.List;
public interface UserDao {

    User registerUser(User user);

    User loadByName(String username);

    List<Order> getOrders(String userId);

    int addOrder(String userId, String description, String status, String expectedDate);

    boolean changeOrder(String id, String description);

    String getFullName(String userId);

    Order getOrder(String orderId, String userId);
}
