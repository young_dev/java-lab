package com.epam.lab.publicutilities.dao;

import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.User;
import com.epam.lab.publicutilities.model.Worker;

import java.util.List;
import java.util.Map;

public interface AdminDao {
    List<User> getUser(String userId);
    List<User> getUsers();
    List<Map<Order, String>> getOrders();
    List<Worker> getWorkers();
    boolean changeUserRole(String userId, String newRole);
    boolean deleteRequest(String orderId);
    boolean deleteWorker(String workerId);
    boolean deleteUser(String userId);
}
