package com.epam.lab.publicutilities.dao.impl;


import com.epam.lab.publicutilities.dao.DispatcherDao;
import com.epam.lab.publicutilities.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Repository
@PropertySource("classpath:queries/DispatcherQuery.properties")
public class DispatcherDaoImpl extends NamedParameterJdbcTemplate implements DispatcherDao {
    private WorkerRowMaper workerRowMaper;

    @Value("${GET_ALL_USERS}")
    String getAllUsers;
    @Value("${GET_ALL_ORDERS}")
    String getAllOrders;
    @Value("${GET_ALL_WORKERS}")
    String getAllWorkers;
    @Value("${GET_ALL_WORKPLANS}")
    String getAllWorkplans;
    @Value("${GET_ORDER_BY_ID}")
    String getOrderById;
    @Value("${GET_ALL_BRIGADES}")
    String getAllBrigades;
    @Value("${CREATE_NEW_FORMED_BRIGADE}")
    String createFormedBrigade;
    @Value("${CREATE_NEW_WORKPLAN}")
    String createWorkPlan;
    @Value("${CREATE_NEW_BRIGADE}")
    String createBrigade;
    @Value("${UPDATE_ORDER_STATUS_BY_ID}")
    String changeOrderStatusById;
    @Value("${UPDATE_WORKER_STATUS_BY_ID}")
    String changeUserStatusById;
    @Value("${GET_WORKER_BY_ID}")
    String getWorkerById;

    public DispatcherDaoImpl(DataSource dataSource){

        super(dataSource);
        this.workerRowMaper = new WorkerRowMaper();
    }

    @Override
    public List<FormedBrig> getAllBrigades(){
        return query(getAllBrigades, (rs, rowNum) -> new FormedBrig(
                rs.getInt("ID"),
                rs.getInt("WORKER_ID"),
                rs.getInt("BRIG_ID")));
    }

    @Override
    public List<Order> getAllOrders(){
        return query(getAllOrders, (rs, rowNum) -> new Order(
                rs.getInt("ID"),
                rs.getInt("USER_ID"),
                rs.getString("DESCRIPTION"),
                Status.valueOf(rs.getString("STATUS")),
                rs.getDate("EXPECTED_DATE")));
    }

    @Override
    public List<User> getAllUsers(){
        return query(getAllUsers, (rs, rowNum) -> new User(
                rs.getInt("ID"),
                rs.getString("FULLNAME"),
                rs.getString("ADDRESS"),
                Role.valueOf(rs.getString("ROLE"))
                ));
    }

    @Override
    public List<Worker> getAllWorkers(){
        return query(getAllWorkers, (rs, rowNum) -> new Worker(
                rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getString("SPECIALIZATION"),
                rs.getBoolean("ISFREE")
        ));
    }

    @Override
    public List<WorkingPlan> getAllWorkPlans() {
        return query(getAllWorkplans, (rs, rowNum) -> new WorkingPlan(
                rs.getInt("ID"),
                rs.getInt("ORDER_ID"),
                rs.getInt("ID_BRIG"),
                rs.getDate("DEADLINE"),
                rs.getString("DESCRIPTION")
        ));
    }

    @Override
    public Order getOrder(int orderId) {
        return query(getOrderById, (rs, rowNum) -> new Order(
                rs.getInt("ID"),
                rs.getInt("USER_ID"),
                rs.getString("DESCRIPTION"),
                Status.valueOf(rs.getString("STATUS")),
                rs.getDate("EXPECTED_DATE"))).get(0);
    }

    @Override
    public Worker getWorkerById(int workerId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("ID", workerId);
        return queryForObject(getWorkerById, parameterSource, workerRowMaper);
    }

    @Override
    public boolean changeOrderStatus(String orderStatus, int orderId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("STATUS", orderStatus);
        parameterSource.addValue("ID", orderId);

        return update(changeOrderStatusById, parameterSource) == 1;
    }

    @Override
    public boolean changeWorkerStatus(int workerId, int status) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("ID", workerId);
        parameterSource.addValue("ISFREE", status);
        return update(changeUserStatusById, parameterSource) == 1;
    }

    @Override
    public FormedBrig createNewFormedBrigade(int workerId, int brigadeId){
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("WORKER_ID", workerId);
        parameterSource.addValue("BRIG_ID", brigadeId);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        update(createFormedBrigade, parameterSource, keyHolder);
        return new FormedBrig(keyHolder.getKey().intValue(), workerId, brigadeId);
    }

    @Override
    public WorkingPlan createNewWorkingPlan(int orderId, int brigId, Date deadline, String description){
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("ORDER_ID", orderId);
        parameterSource.addValue("ID_BRIG", brigId);
        parameterSource.addValue("DEADLINE", deadline);
        parameterSource.addValue("DESCRIPTION", description);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        update(createWorkPlan, parameterSource, keyHolder);

        return new WorkingPlan(keyHolder.getKey().intValue(), orderId, brigId, deadline, description);

    }

    @Override
    public Brig createNewBrigade(int brigadeId){
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("NUMBER", brigadeId);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        update(createBrigade, parameterSource, keyHolder);
        return new Brig(keyHolder.getKey().intValue(), brigadeId);
    }

    private static class WorkerRowMaper implements RowMapper<Worker> {
        @Override
        public Worker mapRow(ResultSet rs, int rowNum) throws SQLException {
            Worker worker = new Worker();
            worker.setId(rs.getInt("ID"));
            worker.setName(rs.getString("NAME"));
            worker.setSpecialization(rs.getString("SPECIALIZATION"));
            worker.setFree(rs.getBoolean("ISFREE"));
            return worker;
        }
    }

}
