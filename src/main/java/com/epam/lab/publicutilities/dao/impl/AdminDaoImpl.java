package com.epam.lab.publicutilities.dao.impl;

import com.epam.lab.publicutilities.dao.AdminDao;
import com.epam.lab.publicutilities.model.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Repository
@ComponentScan(basePackages = { "com.epam.lab.publicutilities.*" })
@PropertySource("classpath:queries/AdminQuery.properties")
@FieldDefaults(level=AccessLevel.PRIVATE)
public class AdminDaoImpl extends NamedParameterJdbcTemplate implements AdminDao {
    @Value("${SELECT_USERS}")
    String SELECT_USERS;
    @Value("${SELECT_USER}")
    String SELECT_USER;
    @Value("${SELECT_ORDERS}")
    String SELECT_ORDERS;
    @Value("${SELECT_WORKERS}")
    String SELECT_WORKERS;
    @Value("${DELETE_USER}")
    String DELETE_USER;
    @Value("${DELETE_WORKER}")
    String DELETE_WORKER;
    @Value("${DELETE_ORDER}")
    String DELETE_ORDER;
    @Value("${UPDATE_USERROLE}")
    String UPDATE_USERROLE;

    public AdminDaoImpl(DataSource dataSource){
        super(dataSource);
    }

    RowMapper<User> userFunc = (rs, rm) -> new User(
                  rs.getInt("ID"),
                  rs.getString("FULLNAME"),
                  rs.getString("ADDRESS"),
            Role.valueOf(rs.getString("ROLE")));


    @Override
    public List<User> getUser(String userId) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", userId);
        return query(SELECT_USER, namedParameters, userFunc);
    }

    @Override
    public boolean changeUserRole(String userId, String newRole) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", userId);
        namedParameters.addValue("role", newRole);
        return update(UPDATE_USERROLE, namedParameters) == 1;
    }

    @Override
    public List<Map<Order, String>> getOrders() {
        return query(SELECT_ORDERS, (rS, rN) -> {
            Map<Order, String> h = new HashMap<>();
            h.put(new Order(
                    rS.getInt("ID"),
                    rS.getInt("USER_ID"),
                    rS.getString("DESCRIPTION"),
                    Status.valueOf(rS.getString("STATUS")),
                    rS.getDate("EXPECTED_DATE")), rS.getString("FULLNAME"));
            return h;
        });
    }

    @Override
    public boolean deleteRequest(String orderId) {
        SqlParameterSource namedParameters =
                new MapSqlParameterSource("orderid", orderId);
        return update(DELETE_ORDER, namedParameters) == 1;
    }

    @Override
    public List<Worker> getWorkers() {
        return query(SELECT_WORKERS, (rS, rN) -> new Worker(
                rS.getInt("ID"),
                rS.getString("NAME"),
                rS.getString("SPECIALIZATION"),
                rS.getBoolean("ISFREE")));
    }

    @Override
    public boolean deleteWorker(String workerId) {
        SqlParameterSource namedParameters =
                new MapSqlParameterSource("workerid", workerId);
        return update(DELETE_WORKER, namedParameters) == 1;
    }

    @Override
    public List<User> getUsers() {
        return query(SELECT_USERS, userFunc);
    }

    @Override
    public boolean deleteUser(String userId) {
        SqlParameterSource namedParameters =
                new MapSqlParameterSource("userid", userId);
        return update(DELETE_USER, namedParameters) == 1;
    }
}
