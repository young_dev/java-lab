package com.epam.lab.publicutilities.util;

import com.epam.lab.publicutilities.model.Role;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;

public class SecurityUtils {

    public static String getUserHomepage(Collection<? extends GrantedAuthority> userRoles) {
        for (GrantedAuthority role : userRoles) {
            if (role.getAuthority().equalsIgnoreCase(Role.ROLE_ADMIN.name())) {
                return "/admin";
            } else if (role.getAuthority().equalsIgnoreCase(Role.ROLE_DISPATCHER.name())) {
                return "/dispatcher";
            } else if (role.getAuthority().equalsIgnoreCase(Role.ROLE_USER.name())) {
                return "/user/orders/";
            }
        }
        return "login";
    }
}
