package com.epam.lab.publicutilities.service;

import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.User;
import com.epam.lab.publicutilities.model.Worker;

import java.util.List;
import java.util.Map;

public interface AdminService {
    List<User> getUsers();
    List<Worker> getWorkers();
    List<Map<Order, String>> getOrders();
    List<User> getUser(String userId);
    boolean deleteUser(String userId);
    boolean deleteRequest(String requestId);
    boolean deleteWorker(String workerId);
    boolean changeUserRole(String userId, String newRole);
}
