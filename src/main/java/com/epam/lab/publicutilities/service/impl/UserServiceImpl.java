package com.epam.lab.publicutilities.service.impl;

import com.epam.lab.publicutilities.dao.UserDao;
import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.Role;
import com.epam.lab.publicutilities.model.Status;
import com.epam.lab.publicutilities.model.User;
import com.epam.lab.publicutilities.service.UserService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserServiceImpl implements UserService {

    UserDao userDao;
    PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User registerUser(String username, String password, String fullName, String address) {
        User user = new User();
        user.setUsername(username);
        user.setFullName(fullName);
        user.setAddress(address);
        String encodedPassword = passwordEncoder.encode(password);
        user.setPassword(encodedPassword);
        user.setRole(Role.ROLE_USER);
        return userDao.registerUser(user);
    }

    @Override
    public User loadUserByName(String username) {
        return userDao.loadByName(username);
    }
    @Override
    public List<Order> getOrders(String userId) {
        return userDao.getOrders(userId);
    }

    @Override
    public int addOrder(String userId, String description, String status, String expectedDate) {
        return userDao.addOrder(userId, description, status, expectedDate);
    }

    @Override
    public boolean changeOrder(String id, String description) {
        return userDao.changeOrder(id, description);
    }

    @Override
    public String getFullName(String userId) {
        return userDao.getFullName(userId);
    }

    @Override
    public Order getOrder(String orderId, String userId) {
        return userDao.getOrder(orderId, userId);
    }
}
