package com.epam.lab.publicutilities.service.impl;


import com.epam.lab.publicutilities.dao.DispatcherDao;
import com.epam.lab.publicutilities.model.*;
import com.epam.lab.publicutilities.service.DispatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DispatcherServiceImpl implements DispatcherService {

    private final DispatcherDao dispatcherDao;

    @Autowired
    public DispatcherServiceImpl(DispatcherDao dispatcherDao) {
        this.dispatcherDao = dispatcherDao;
    }

    @Override
    public List<Order> getAllOrders() {
        return dispatcherDao.getAllOrders();
    }

    @Override
    public List<Worker> getAllWorkers() {
        return dispatcherDao.getAllWorkers();
    }

    @Override
    public List<User> getAllUsers() {
        return dispatcherDao.getAllUsers();
    }

    @Override
    public List<WorkingPlan> getAllWorkPlans() {
        return dispatcherDao.getAllWorkPlans();
    }

    @Override
    public List<FormedBrig> getAllBrigades() {
        return  dispatcherDao.getAllBrigades();
    }

    @Override
    public Order getOrder(int orderId) {
        return dispatcherDao.getOrder(orderId);
    }

    @Override
    public FormedBrig createNewFormedBrigade(List<Integer> workersIds, int brigId) {
        for(Integer workerId : workersIds){
            return dispatcherDao.createNewFormedBrigade(workerId, brigId);
        }
        return new FormedBrig();
    }

    @Override
    public WorkingPlan createNewWorkingPlan(int orderId, int brigId, Date deadline, String description) {
        return dispatcherDao.createNewWorkingPlan(orderId, brigId, deadline, description);
    }

    @Override
    public Boolean changeOrderStatus(String orderStatus, int orderId) {
        return dispatcherDao.changeOrderStatus(orderStatus, orderId);
    }

    @Override
    public boolean changeWorkersStatuses(List<Integer> workersIds, int status) {
        for(Integer workerId : workersIds) {
           dispatcherDao.changeWorkerStatus(workerId, status);
        }
        return true;
    }

    @Override
    public Map<Integer, String> getUserIdNameMap() {
        List<User> users = dispatcherDao.getAllUsers();
        Map<Integer, String> userIdToNameMap = new HashMap<>();
        for(User user : users) {
            userIdToNameMap.put(user.getId(), user.getFullName());
        }
        return userIdToNameMap;
    }

    @Override
    public List<Worker> getWorkersListByIds(List<Integer> workersIds) {
        List<Worker> workers = new ArrayList<>();
        for (Integer workerId : workersIds){
            workers.add(dispatcherDao.getWorkerById(workerId));
        }
        return workers;
    }

    @Override
    public Worker getWorkerById(int workerId) {
        return dispatcherDao.getWorkerById(workerId);
    }

    @Override
    public boolean changeWorkerStatus(int workerId, String workerStatus) {
        if(workerStatus.equals("TRUE")){
            return dispatcherDao.changeWorkerStatus(workerId, 1);
        } else
            return dispatcherDao.changeWorkerStatus(workerId, 0);

    }

    @Override
    public Brig createNewBrigade(int brigadeId) {
        return dispatcherDao.createNewBrigade(brigadeId);
    }
}
