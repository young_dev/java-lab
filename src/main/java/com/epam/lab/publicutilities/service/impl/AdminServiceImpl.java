package com.epam.lab.publicutilities.service.impl;

import com.epam.lab.publicutilities.dao.AdminDao;
import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.User;
import com.epam.lab.publicutilities.model.Worker;
import com.epam.lab.publicutilities.service.AdminService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminServiceImpl implements AdminService {
    AdminDao adminDao;

    @Override
    public List<User> getUsers() {
        return adminDao.getUsers();
    }

    @Override
    public List<User> getUser(String userId) {
        return adminDao.getUser(userId);
    }

    @Override
    public List<Worker> getWorkers() {
        return adminDao.getWorkers();
    }

    @Override
    public List<Map<Order, String>> getOrders() {
        return adminDao.getOrders();
    }

    @Override
    public boolean deleteRequest(String requestId) {
        return adminDao.deleteRequest(requestId);
    }

    @Override
    public boolean changeUserRole(String userId, String newRole) {
        return adminDao.changeUserRole(userId, newRole);
    }

    @Override
    public boolean deleteUser(String userId) {
        return adminDao.deleteUser(userId);
    }

    @Override
    public boolean deleteWorker(String workerId) {
        return adminDao.deleteWorker(workerId);
    }
}
