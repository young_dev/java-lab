package com.epam.lab.publicutilities.service;

import com.epam.lab.publicutilities.model.*;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface DispatcherService {
    List<Order> getAllOrders();
    List<Worker> getAllWorkers();
    List<User> getAllUsers();
    List<WorkingPlan> getAllWorkPlans();
    List<FormedBrig> getAllBrigades();
    Order getOrder(int orderId);
    Brig createNewBrigade(int brigadeId);
    FormedBrig createNewFormedBrigade(List<Integer> workerId, int brigId);
    WorkingPlan createNewWorkingPlan(int orderId, int brigId, Date deadline, String description);
    Boolean changeOrderStatus(String orderStatus, int orderId);
    boolean changeWorkersStatuses(List<Integer> workerIds, int status);
    Map<Integer, String> getUserIdNameMap();
    List<Worker> getWorkersListByIds(List<Integer> workersIds);

    Worker getWorkerById(int workerId);

    boolean changeWorkerStatus(int workerId, String workerStatus);
}
