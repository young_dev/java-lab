package com.epam.lab.publicutilities.service;

import com.epam.lab.publicutilities.model.Order;
import com.epam.lab.publicutilities.model.Status;
import com.epam.lab.publicutilities.model.User;

import java.util.List;

public interface UserService {

    User registerUser(String username, String password, String fullname, String address);
    User loadUserByName(String username);

    List<Order> getOrders(String userId);

    int addOrder(String userId, String description, String status, String expectedDate);

    boolean changeOrder(String id, String description);

    String getFullName(String userId);

    Order getOrder(String orderId, String userId);
}
